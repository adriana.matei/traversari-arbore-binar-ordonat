#include<stdio.h>
#include<stdlib.h>
#include<time.h>
struct Nod{
    int date;
    struct Nod*stanga;
    struct Nod*dreapta;
};
struct Nod*creareNod(int valoare)
{
    struct Nod*nodNou=(struct Nod*)malloc(sizeof(struct Nod));
    nodNou->date=valoare;
    nodNou->dreapta=NULL;
    nodNou->stanga=NULL;
    return nodNou;
}
struct Nod*inserareNod(struct Nod*radacina,int valoare)
{
    if(radacina==NULL)
        return creareNod(valoare);
    if(valoare<radacina->date)
    {
        radacina->stanga=inserareNod(radacina->stanga,valoare);
    }
    else if(valoare>radacina->date)
    {
        radacina->dreapta=inserareNod(radacina->dreapta,valoare);
    }
    return radacina;
}
void generareElementeRandom(struct Nod**radacina,int numarElemente,int*valoriRandom)
{
    int i;
    srand(time(NULL));
    printf("\nValorile generate aleatoriu sunt: ");
    for(i=0;i<numarElemente;i++)
    {
            int valoareRandom=rand() % 100;
            valoriRandom[i]=valoareRandom;
            printf("%d ",valoareRandom);
            *radacina=inserareNod(*radacina,valoareRandom);
    }
    printf("\n");
}
void preordine(struct Nod*radacina)
{
    if(radacina==NULL)
                 return;
    printf("%d ",radacina->date);
    preordine(radacina->stanga);
    preordine(radacina->dreapta);
}
void inordine(struct Nod*radacina)
{
    if(radacina==NULL)
        return ;
    inordine(radacina->stanga);
    printf("%d ",radacina->date);
    inordine(radacina->dreapta);
}
void postordine(struct Nod*radacina)
{
    if(radacina==NULL)
     return;
    postordine(radacina->stanga);
    postordine(radacina->dreapta);
    printf("%d ",radacina->date);
}
void nivel(struct Nod* radacina) {
    if (radacina == NULL)
        return;

    struct Nod* queue[100];
    int front = 0, rear = 0;

    queue[rear++] = radacina;

    while (front < rear) {
        struct Nod* current = queue[front++];
        printf("%d ", current->date);

        if (current->stanga != NULL)
            queue[rear++] = current->stanga;
        if (current->dreapta != NULL)
            queue[rear++] = current->dreapta;
    }
}
int main()
{
    struct Nod*radacina=NULL;
    int numarElemente;
    int optiune;
   do{
        printf("\nIntroduceti numarul de elemente dorit(trebuie sa fie o valoare pozitiva): ");
        scanf("%d",&numarElemente);
        if(numarElemente<0)
        {
            printf("\nAti introdus o valore gresita");
        }
    }while(numarElemente<0);
      int*valoriRandom=(int*)malloc(numarElemente*sizeof(int));
      generareElementeRandom(&radacina,numarElemente,valoriRandom);
      do{
           printf("\nMeniu:");
           printf("\n1.Treaversare preordine");
           printf("\n2.Traversare inordine");
           printf("\n3.Traversare postordine");
           printf("\n4.Traversare pe nivele");
           printf("\n0.Iesire");
           printf("\nIntroduceti optiunea dorita: ");
           scanf("%d",&optiune);
           switch (optiune)
           {
           case 0:printf("\nIesire\n");
           exit(0);
            break;
           case 1:printf("\nPreordine: ");
			preordine(radacina);
			printf("\n");
			break;
            case 2:printf("\nInordine: ");
            inordine(radacina);
            printf("\n");
            break;
            case 3:printf("\nPostordine: ");
            postordine(radacina);
            printf("\n");
            break;
            case 4:printf("\nPe nivele: ");
            nivel(radacina);
            printf("\n");
            break;
           default:printf("\nOptiune invalida!\n");
           printf("\nAlegeti o alta optiune \n");
            break;
           }
      }while(1);
    free(valoriRandom);
    return 0;
}
