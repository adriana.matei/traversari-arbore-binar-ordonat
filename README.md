# Implemenatrea traversarilor intr-un arbore binar ordonat cu elemente genearate aleatoriu
## Name
Implemenatrea traversarilor intr-un arbore binar ordonat cu elemente genearate aleatoriu

## Description
Arborii binari ordonati sunt o structură de date importantă în informatică. Ei sunt utili pentru stocarea și căutarea datelor, precum și pentru implementarea altor algoritmi eficienți. Un arbore binar ordonat este un arbore în care fiecare nod are cel mult doi copii, stângul și dreptul. Cheia unui nod este mai mică decât cheia oricărui nod din subarborele său stâng și mai mare decât cheia oricărui nod din subarborele său drept.
Intr-un arbore binar ordonat elementele sunt introduse o singura data,neexistand elemente duplicate.
## Detalii de implementare
Acest proiect implementeaza un program in limbajul C pentru traversarea unui arbore binar ordonat,populat cu elemente generate aleatoriu.Programul permite utilizatorului să introducă un număr specific de elemente, generează aleatoriu acele elemente și le inserează într-un arbore binar ordonat. Acest arbore este apoi utilizat pentru a realiza diferite tipuri de traversări, oferind vizualizarea conținutului în ordine predefinită. Structura arborelui și funcțiile asociate permit manipularea eficientă a datelor și realizarea traversărilor în mod corespunzător.
Există trei moduri principale de a traversa un arbore binar ordonat:

Preordine: În traversarea preordine, nodul curent este vizitat primul, urmat de subarborele său stâng și apoi de subarborele său drept.

Inordine: În traversarea inordine, subarborele stâng al nodului curent este vizitat primul, urmat de nodul curent însuși și apoi de subarborele său drept.

Postordine: În traversarea postordine, subarborele stâng al nodului curent este vizitat primul, urmat de subarborele său drept și apoi de nodul curent însuși.

Structura Nod reprezintă un nod în arborele binar ordonat, conținând o valoare și doi pointeri către nodurile stâng și drept.
Functia de creare a nodului primește o valoare și returnează un nod nou creat cu acea valoare.
Functia de inserare a nodului in arbore  primește un nod-radacina și o valoare, apoi inserează un nod cu acea valoare în arborele binar ordonat.
Functia de generare a elementelor random generează aleatoriu o serie de valori, le afișează și le inserează în arbore folosind funcția de inserare.In program sunt implementate traversarea in preordine,inordine,postordine si pe nivel.Aceste metode de traversare oferă diferite modalități de explorare și procesare a nodurilor unui arbore binar. Fiecare traversare are propriile sale cazuri de utilizare, iar alegerea depinde de problema sau cerința specifică. De exemplu, traversarea în inordine este adesea folosită pentru afișarea elementelor unui BST în ordine sortată, în timp ce traversările în preordine și postordine pot fi utilizate pentru anumite tipuri de evaluări ale arborilor de expresii sau pentru ordinea specifică de procesare a nodurilor.

Traversarea pe nivele a unui arbore necesită o abordare bazată pe niveluri sau straturi. Utilizarea unei cozi este esențială în acest context datorită modului în care funcționează această metodă de traversare.Principiul de bază al acestei cozi este să asigure un acces eficient la elementul cel mai vechi adăugat, respectând principiul FIFO(First In, First Out).

Programul utilizează alocare dinamică pentru a stoca valorile generate aleatoriu, iar memoria este eliberată la sfârșitul programului.
Folosește pointeri și funcții recursive pentru manipularea și traversarea arborelui.
Meniul interactiv permite utilizatorului să aleagă diferite tipuri de traversare pentru a vizualiza conținutul arborelui.

## Motivare si scop
Implementarea traversărilor într-un arbore binar ordonat este esențială pentru accesarea și manipularea elementelor stocate în arbore. Traversările reprezintă procesul de vizitare a fiecărui nod al arborelui într-o anumită ordine. Există trei tipuri principale de traversări într-un arbore binar ordonat: preordine, înordine și postordine. Iată motivele și scopurile pentru implementarea acestora:

1.Accesarea și Afișarea Elementelor

2.Căutare și Găsire
Traversarea înordine permite găsirea unui element specific în ABO, deoarece valorile sunt afișate în ordine crescătoare.
Traversarea preordine sau postordine poate fi folosită pentru a găsi informații despre relațiile dintre noduri sau pentru a efectua anumite operații pe întregul arbore.

3.Eliminarea și Inserarea Elementelor
Traversările ajută la identificarea locului potrivit pentru inserarea unui nou nod în ABO sau pentru eliminarea unui nod existent.
Permite identificarea succesorului sau predecesorului unui nod pentru a efectua corect eliminarea.

4.Calcularea și Analiza Arborelui
Traversările pot fi folosite pentru a calcula înălțimea arborelui, numărul total de noduri sau alte proprietăți ale acestuia.
Ajută în analiza structurii și performanței arborelui, furnizând informații despre distribuția și organizarea elementelor.

5.Procesare în Timp Real
Traversările pot fi folosite pentru a procesa elementele din arbore în timp real, fără a fi necesară stocarea întregului arbore în memorie.
Sunt utile în situațiile în care trebuie să se manipuleze sau să se afișeze date pe măsură ce sunt disponibile.
## Installation
Acest program este scris în limbajul C și poate fi compilat folosind un compilator standard de C. Urmați acești pași:
1. Asigurați-vă că aveți un compilator de C instalat pe sistemul dvs.
2. Clonați depozitul pe mașina locală.
3. Compilați programul.
4. Rulați executabilul.

## Usage
1. Introduceți numărul de elemente pe care doriți să le aveți în arborele binar la solicitare.
2. Alegeți tipul de traversare din meniu.
3. Vizualizați rezultatele traversării selectate.
- Compilator de C (de exemplu:GCC)
- Biblioteci standard de C
## Exemplu de rulare
$ gcc proiect.c -o arbore_binar
$ ./arbore_binar
Introduceti numarul de elemente dorit(trebuie sa fie o valoare pozitiva): 5
Valorile generate aleatoriu sunt: 42 18 79 11 56

Meniu:
1. Treaversare preordine
2. Traversare inordine
3. Traversare postordine
4. Traversare pe nivele
0. Iesire
Introduceti optiunea dorita: 2

Inordine: 11 18 42 56 79

-Utilizatorul poate iesi din meniu introducand optiunea 0
Meniu:
1. Treaversare preordine
2. Traversare inordine
3. Traversare postordine
4. Traversare pe nivele
0. Iesire
Introduceti optiunea dorita: 0

Iesire

## Mediul de dezvoltare
Limbaj:C
IDE:Visual Studio Code
## Roadmap
-Imbunatatirea perfomantei algoritmului
-Imbunatatirea interactiunii cu utilizatorul
## Authors 
Acest proiect a fost realizat de Matei Adriana-Ionela

## Bibliografie
http://staff.cs.upt.ro/~chirila/teaching/upt/id-aa/lectures/AA-ID-Cap08-2.pdf
https://www.pbinfo.ro/articole/25641/arbori-binari